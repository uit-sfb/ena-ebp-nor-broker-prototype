import yaml, os, requests, itertools, argparse, json
from collections import OrderedDict, defaultdict
from brokering.api import *
from brokering.xml import *
from brokering.map import *
from brokering.ena import *

class Context:
    '''
    Holds all variables and functions related to settings, attributes, username, passwords, api-urls etc.

    self.dev - Flag that switches between testdev (True) and prod (False). Defaults to Flase.
    self.id - Holds the accession for the specific EBP-Nor Isolate entry staged for submission
    self.careface - Flag that indicates whether to keep on going on errors while self.dev == True. This is convenient as you can not delete anything from the testdev repo, which makes testing script-logic hard (Entries are removed from testdev every 24 hours only)
    self.assembly - Flag needed to also submit assembly files. TODO
    self.verbose - Prints all kinds of output convenient for development / debugging
    self.output_folder - Defines the output folder where files, logs etc. are stored
    self.ena_user - Sets your ENA USER/PASS from the environment variables $ENA_USER and $ENA_PWD
    self.ena_pwd - Sets your ENA USER/PASS from the environment variables $ENA_USER and $ENA_PWD
    self.xmlfile - Deprecated for noow
    self.configfile - YAML file defining specific attributes from the EBP-Nor api to retrieve
    self.attributes - Holds parsed yaml file
    self.data = Default Dict that holds transformed API data (nested dict)
    '''
    def __init__(self, configfile, id, dev=False, careface=False, assembly=False, verbose=False, xmlfile="submission.xml") -> None:
        self.dev = dev
        self.id = id
        self.careface = careface if self.dev==True else False
        self.assembly = assembly
        self.verbose = verbose
        self.output_folder = self.make_submission_dir()
        self.ena_user = os.environ["ENA_USER"]
        self.ena_pwd = os.environ["ENA_PWD"]
        self.xmlfile = xmlfile
        self.configfile = configfile
        self.attributes = self.load_config(self.configfile)
        self.data = defaultdict(str)

        if dev:
            self.retrieveurl = 'https://wwwdev.ebi.ac.uk/ena/submit/webin-v2/sample/'
            self.submiturl = 'https://wwwdev.ebi.ac.uk/ena/submit/webin-v2/submit/queue'
            self.pollurl = 'https://wwwdev.ebi.ac.uk/ena/submit/webin-v2/submit/poll/'
        else:
            # Not implemented yet. Comes last, as safety precaution
            pass
            #self.submiturl = 'https://www.ebi.ac.uk/ena/submit/webin-v2/'


    def load_config(self, config):
        '''
        Returns config-object from config.yaml, mapping database divisions and needed attributes for a valid ENA-submission.
        '''
        with open(config) as f:
            config = yaml.load(f, Loader=yaml.loader.SafeLoader)
            return (config)

    def make_submission_dir(self):
        if not os.path.exists(os.path.join(os.getcwd(), 'SUBMISSION')):
            os.makedirs(os.path.join(os.getcwd(), 'SUBMISSION'))
        return os.path.join(os.getcwd(), 'SUBMISSION')


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Prototype brokering implementation from EBP-Nor to ENA. Make sure you have USER/PASS to ENA set as environment variables: $ENA_USER and $ENA_PWD")
    parser.add_argument("--dev", help="Mock publishing using ENA test API - (default: False)", action="store_true", default=False)
    parser.add_argument("--careface", help="Skips some tests. Requires dev==True - (default: False)", action="store_true", default=False)
    parser.add_argument("--assembly", help="(TODO:) Also submit Assembly - (default: False)", action="store_true", default=False)
    parser.add_argument("--verbose", help="Prints various debugging outputs - (default: False)", action="store_true", default=False)
    parser.add_argument("-y", "--yaml", help="YAML definitions file (default: 'brokering/config.yaml')", default="brokering/config.yaml")
    parser.add_argument("-i", "--id", help="EBP-Nor Speciemen database id of submission", required=True)
    args = parser.parse_args()

    ### Data retrieval and transformation ###
 
    # Instantiate Context object, which holds all settings and needed attributes 
    config = Context(args.yaml, args.id, args.dev, args.careface, args.assembly, args.verbose)
    
    # Dict which will holds all json-simplenamespace objects that has relations to the specific specimen id resource. Attributes for XML will be picked from this later.
    data = OrderedDict()

    # We use speciemen as an entrypoint, as this should be unique pr submission, and has relations to other necessary tables except assembly, which is a special case anyway
    accession = 'ebpnor.specimen:'+config.id
    data[accession] = fetch_id_from_database("ebp-nor_specimen", config.id, config)

    # We build a list with relations to other db-tables based on the retrieved specimen jsondata from the API
    links = get_dblinks_from_jsondata(data[accession])
    
    # Based on links list, get relational data from other tables, add to data object
    for link in links:
        # transform "ebpnor.db:accession" to "ebp_nor:db:accession" for api retrieval
        dbname, id = link.split(":")
        dbname = dbname.split(".")[1]
        dbname = "ebp-nor_" + dbname

        # (specimen already exists from before)
        if not link in data.keys():
            data[link] = fetch_id_from_database(dbname, id, config)

    # TODO: Add assembly at some point. (links from sequencing, fetch from ebp-nor_assembly)
    if config.assembly:
        pass

    # Print status so far if verbose is TRUE
    if config.verbose:
        print('\033[0;32m'+"Your submission is based on the following ebp-nor accessions. Please double check this: "+'\033[0m')
        print(accession)
        for entry in links:
            print(entry)

    # Map retrieved data from API to attributes parsed from config.yaml
    map_to_config(data, config)

    ### Data submission workflow (TODO) ###

    # 1. Check for parent project. If this does not exist from the EBP-Nor API, 'species' <PROJECT> in ENA does not exist an this is the first submission of its kind. Create this and submit.
    if not config.data['ebpnor.isolate']['adm:parent_pro']:
        if config.verbose == True:
            print('\033[0;32m'+"Parent project is missing. Submitting 'Species' level xml "+'\033[0m')
        # Create xml for top level species with species=True
        xml = tranform_to_xml(config, species=True)
        write_xml(xml, config, "species_project.xml")
        receipt = submit_metadata(config, "species_project.xml")
        poll_metadata(config, receipt, "species_project.xml")
    
    # 2. At this point, species level project exists in ENA. Submit isolate <PROJECT> to XML and submit
    if config.verbose == True:
            print('\033[0;32m'+"Submitting 'Isolate' level xml "+'\033[0m')
    # Crete xml for top level species with species=True
    xml = tranform_to_xml(config, isolate=True)
    write_xml(xml, config, "isolate_project.xml")
    receipt = submit_metadata(config, "isolate_project.xml")
    poll_metadata(config, receipt, "isolate_project.xml")

    # 3. transform <SAMPLE> to XML and submit


