# EBP-Nor Brokering Prototype implementation

## How-To

Run the main.py script with an accession from the EBP-Nor isolate db as an entry point. Run in dev-mode to test, ex:  
`python main.py -i 000000002 --dev`

This generates a `submission.xml` file which can be submitted to ENA apiv2

(use `--verbose` to inspect data from different stages and `--prettify` to produce a "human readable" XML. A prettyied XML can not be submitted due to strict validation regexp on ENA's end)

## Flow of actions in brokering pipeline

### Static/Needs to be in place (Creates needed Umbrella projexts. Happens once)

1. EBP-Nor Project needs to exist beforehand. If not, this can be created using:
   `./example_curl_to_ena_submit.sh XML_EXAMPLES/CREATE_TOP_UMBRELLA.xml`
2. EBP-Nor Project needs to exist beforehand. If not, this can be created using:

### Dynamic (Actions performed by brokering service)

1. Check whether EBP-Nor TOP_UMBRELLA exists in ENA based on input ERS accesion, if not raises exception.
2. Check whether EBP-Nor
3. Check whether species exsts in ENA, if not submit xml creating species:
   --> CREATE_SPECIES_EXAMPLE.xml (**TODO**: Implementation)
4. Upload files using webin-client from Nels (**TODO**: Implementation)
5. Generated submission XML using `main.py` (**Done**)
6. Submit submission.xml using curl/main.py (**DONE**)
