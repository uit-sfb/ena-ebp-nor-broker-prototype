import urllib3, json, itertools
from types import SimpleNamespace
from http.client import responses

def query_api(url, config):
    ''' 
    Returns ORM-like SimpleNamespace mapped from API provided JSON-object
    '''
    with urllib3.PoolManager().request("GET",url,preload_content=False) as query:
        if not (query.status == 200):
            status = responses[query.status]
            error = "{} - {}".format(query.status, status)
            raise Exception(error)
        else:
            if config.verbose:
                print('\033[0;32m'+"query_api(): "+url+'\033[0m')
                print (str(url)+"\n"+str(query.data))
            jsondata = json.loads(query.data,object_hook=lambda x: SimpleNamespace(**x))
    return jsondata

def build_api_url(division,version="1.1"):
    '''
    Returns API-string for urllib relative to db-version and discrete db-division provided
    (Not used in this specific implementation - serves as an example)
    '''
    division, version = division, version
    prefix = "https://databasesapi.sfb.uit.no/"
    route = "rest/v1/{0}/records?ver={1}".format(division, version)
    params = "&page%5Bsize%5D=100000&page%5Boffset%5D=0&format=raw"
    url = prefix + route + params
    return url

def build_api_url_for_record(division, record, version="1.1"):
    '''
    Returns API-string for urllib relative to db-version, discrete db-division and entry id
    '''
    division, record, version = division, record, version
    prefix = "https://databasesapi.sfb.uit.no/"
    route = "rest/v1/{0}/records/{1}".format(division, record)
    params = "?ver={0}&format=raw".format(version)
    url = prefix + route + params
    return url

def fetch_id_from_database(database, id, config):
    '''
    Returns an object holding necessary data retrieved from the API
    '''
    url = build_api_url_for_record(database, id)
    jsondata = query_api(url, config)
    return jsondata

def get_dblinks_from_jsondata(jsondata):
    links = []
    for link in jsondata.attrs:
        if hasattr(link, 'dblink:isolate'):
            attr = getattr(link, 'dblink:isolate')
            links.append(attr.obj[0].iri)
        if hasattr(link, 'dblink:species'):
            attr = getattr(link, 'dblink:species')
            links.append(attr.obj[0].iri)
        if hasattr(link, 'dblink:sequencing'):
            attr = getattr(link, 'dblink:sequencing')
            links.append(attr.obj[0].iri)
        #if hasattr(link, 'dblink:assembly') and config.assembly == True:
        #    attr = getattr(link, 'dblink:assembly')
        #    links.append(attr.obj[0].iri)
        if hasattr(link, 'dblink:specimen'):
            attr = getattr(link, 'dblink:specimen')
            links.append(attr.obj[0].iri)
    links = sorted(list(itertools.chain(*links)), reverse=True)
    return links