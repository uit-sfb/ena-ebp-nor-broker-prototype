from collections import defaultdict

def map_to_config(data, config):
    '''
    Populates Context.data dict of dict with data from retrieved api (list of jsondata) cross-referenced by Context.attributes (parsed yaml file)
    (if db.keys() == sequencing is a special case as if probably has 2 entries.)
    '''
    # This needs a rewrite.
    # ebpnor.sequencing will have an extra nested layer pr sequencing technology
    seq_type_counter = 0
    for db in data.keys():
        if db.startswith("ebpnor.specimen"):
            crossref = db.split(":")[0]
            # Make default dict of dict if it does not exist (first iteration)
            if not config.data[crossref]:
                config.data[crossref] = defaultdict(str)
            # iterate through parsed yaml attributes
            for xattr in config.attributes[crossref]:
                # Iterate through json-api attributes
                for yattr in data[db].attrs:
                    # When matched, convert insane json structure to simple default dict of dict data structure
                    if hasattr(yattr, xattr):
                        insane = getattr(yattr, xattr)
                        config.data[crossref][xattr] = insane.obj[0].val
        elif db.startswith("ebpnor.species"):
            crossref = db.split(":")[0]
            # Make default dict of dict if it does not exist (first iteration)
            if not config.data[crossref]:
                config.data[crossref] = defaultdict(str)
            # iterate through parsed yaml attributes
            for xattr in config.attributes[crossref]:
                # Iterate through json-api attributes
                for yattr in data[db].attrs:
                    # When matched, convert insane json structure to simple default dict of dict data structure
                    if hasattr(yattr, xattr):
                        insane = getattr(yattr, xattr)
                        if xattr == "tax:id":
                            config.data[crossref][xattr] = insane.obj[0].iri[0]
                        else:
                            config.data[crossref][xattr] = insane.obj[0].val
        elif db.startswith("ebpnor.sequencing"):
            crossref = db.split(":")[0]
            # Make default dict of dict if it does not exist (first iteration)
            if not config.data[crossref]:
                config.data[crossref] = defaultdict(int)
            
            # Make an extra level of nesting with seq platform as unique key
            if not config.data[crossref][seq_type_counter]:
                config.data[crossref][seq_type_counter] = defaultdict(str)
            
                # iterate through parsed yaml attributes
                for xattr in config.attributes[crossref]:
                    # Iterate through json-api attributes
                    for yattr in data[db].attrs:
                        # When matched, convert insane json structure to simple default dict of dict data structure
                        ### Special case if yattr
                        if hasattr(yattr, xattr):
                            insane = getattr(yattr, xattr)
                            #special case for filename and md5sums (same attribute...)
                            if xattr == "seq:filename":
                                if not config.data[crossref][seq_type_counter][xattr]:
                                    config.data[crossref][seq_type_counter][xattr] = []
                                config.data[crossref][seq_type_counter][xattr].append(insane.obj[0].val)
                            elif xattr == "seq:chk_sum":
                                if not config.data[crossref][seq_type_counter][xattr]:
                                    config.data[crossref][seq_type_counter][xattr] = []
                                config.data[crossref][seq_type_counter][xattr].append(insane.obj[0].val)
                            else:
                                config.data[crossref][seq_type_counter][xattr] = insane.obj[0].val
            seq_type_counter =+ 1

        elif db.startswith("ebpnor.isolate"):
            crossref = db.split(":")[0]
            # Make default dict of dict if it does not exist (first iteration)
            if not config.data[crossref]:
                config.data[crossref] = defaultdict(str)
            # iterate through parsed yaml attributes
            for xattr in config.attributes[crossref]:
                # Iterate through json-api attributes
                for yattr in data[db].attrs:
                    # When matched, convert insane json structure to simple default dict of dict data structure
                    if hasattr(yattr, xattr):
                        insane = getattr(yattr, xattr)
                        if xattr == "sam:coordinate" or xattr == "sam:habitat":
                            config.data[crossref][xattr] = insane.obj[0].iri[0]
                        else:
                            config.data[crossref][xattr] = insane.obj[0].val
        if db.startswith("ebpnor.assembly"):
            crossref = db.split(":")[0]
            # Make default dict of dict if it does not exist (first iteration)
            if not config.data[crossref]:
                config.data[crossref] = defaultdict(str)
            # iterate through parsed yaml attributes
            for xattr in config.attributes[crossref]:
                # Iterate through json-api attributes
                for yattr in data[db].attrs:
                    # When matched, convert insane json structure to simple default dict of dict data structure
                    if hasattr(yattr, xattr):
                        insane = getattr(yattr, xattr)
                        config.data[crossref][xattr] = insane.obj[0].val
    if config.verbose:
        print('\033[0;32m'+"config.data: "+'\033[0m')
        for k,v in config.data.items():
            header = '\033[0;32m'+"  {}".format(k)+'\033[0m'
            print (header)
            if k == "ebpnor.sequencing":
                for k,v in v.items():
                    header = '\033[0;32m'+"    {}".format(str(k))+'\033[0m'
                    print (header)
                    for k,v in v.items():
                        print("      "+str(k),str(v))
            else:
                for k,v in v.items():
                    print("    "+str(k),str(v))
