import os, requests, json, time
import urllib.parse
import xml.etree.cElementTree as ET

 
def submit_metadata(config, filename):
    '''
    Submits finished XML to ENA
    '''
    files = {'file': open(os.path.join(config.output_folder, filename),'rb')}
    response = requests.post(config.submiturl, files=files, auth=(config.ena_user, config.ena_pwd))
    if not response.ok:
        raise Exception('\nOops:\n' + response.text)
    receipt = response.content
    # write log
    log_filename = os.path.join(config.output_folder, f'submission.log')
    with open(log_filename, 'a+') as file:
        file.write(f'Submission - {filename}\n')
        file.write(receipt.decode('utf-8'))
        file.write('\n{0}\n'.format('*' * 80))
    return receipt

def poll_metadata(config, receipt, filename):
    '''
    Returns receipt from json.loads(receipt). Raises error if success==False
    '''
    receipt = json.loads(receipt.decode('utf-8'))
    url = receipt['_links']['poll-json']['href']
    response = requests.get(url, auth=(config.ena_user, config.ena_pwd))
    if (not response.ok) and (config.careface==False):
        raise Exception('\nOops:\n' + response.text)
    receipt = response.content
    # For some reason this fails from time to time. Might need a time.sleep for the API to finish processing? Receipt returns malformed/empty somehow.
    time.sleep(5)
    try:
        receipt = json.loads(receipt.decode('utf-8'))
    except:
        print("Exit on transforming receipt from ENA-api to JSON after polling {0}. The receipt might be malformed or empty:\n {1}".format(filename, receipt))
    #print (json.dumps(receipt, indent=4))
    # write log
    log_filename = os.path.join(config.output_folder, f'submission.log')
    with open(log_filename, 'a+') as file:
        file.write(f'Poll - {filename}\n')
        file.write(json.dumps(receipt))
        file.write('\n{0}\n'.format('*' * 80))
    if (receipt['RECEIPT']['success'] == False) and (config.careface==False):
        raise Exception('\nSubmission failed on alias: {0}\n'.format(receipt['RECEIPT']['PROJECT']['alias']) + receipt['RECEIPT']['MESSAGES']['ERROR'])
    
    return receipt

def retrieve_metadata(config, accession):
    url = urllib.parse.urljoin(config.retrieveurl, accession)
    response = requests.get(url, auth=(config.ena_user, config.ena_pwd))
    if not response.ok:
        raise Exception('\nOops:\n' + response.text)
    receipt = response.content
    print(receipt)
    exit()