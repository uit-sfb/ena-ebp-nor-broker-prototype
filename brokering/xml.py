import xml.etree.cElementTree as ET
import os
from bs4 import BeautifulSoup
from datetime import date

def tranform_to_xml(config, actiontype="ADD", species=False, isolate=False, sample=False, experiment=False, run=False):
    '''
    Creates the <WEBIN> root element, and runs discrete functions for each <SUBMISSION_SET>, <PROJECT_SET>, <SAMPLE_SET> ...etc
    Returns XML-formated xml.etree.cElementTree object
    '''
    root = ET.Element("WEBIN")
    root = create_submission_set_xml(config, root, actiontype)
    if species==True:
        root = create_species_project_set_xml(config, root)
    if isolate==True:
        root = create_isolate_project_set_xml(config, root)
    if sample==True:
        root = create_sample_set_xml(config, root)
    if experiment==True:
        root = create_experiment_set(config, root)
    if run==True:
        root = create_run_set(config, root)
    tree = ET.ElementTree(root)
    return tree

def create_submission_set_xml(config, root, actiontype):
    '''
    Returns <SUMBISSON_SET> and all subelements. 
    '''
    submission_set = ET.SubElement(root, "SUBMISSION_SET")
    submission = ET.SubElement(submission_set, "SUBMISSION")
    actions = ET.SubElement(submission, "ACTIONS")
    action = ET.SubElement(actions, "ACTION")
    ET.SubElement(action, actiontype)

    # Add delayed release dato if provided
    action = ET.SubElement(actions, "ACTION")
    ET.SubElement(action, "HOLD", HoldUntilDate=config.data['ebpnor.species']['adm:seq_rel_dt'])

    return root

def create_species_project_set_xml(config, root):
    '''
    Returns <PROJECT_SET> for species umbrella project and all subelements. This is explicily defined to mimic the Darwin tree of life structure in ENA
    This function should only run if this is the first Isolate of its kind (Species umbrella is missing)
    '''
    project_set = ET.SubElement(root, "PROJECT_SET")
    project = ET.SubElement(project_set, "PROJECT", alias=config.data['ebpnor.species']['adm:proj_alias'], center_name=config.data['ebpnor.species']['adm:cent_name'])
    name = ET.SubElement(project, "NAME")
    name.text = config.data['ebpnor.species']['adm:proj_name']
    title = ET.SubElement(project, "TITLE")
    title.text = config.data['ebpnor.species']['adm:proj_title']
    description = ET.SubElement(project, "DESCRIPTION")
    description.text = config.data['ebpnor.species']['adm:proj_title']
    #parent_project = ET.SubElement(project, "PARENT_PROJECT", accession=config.data['ebpnor.species']['adm:parent_pro'])
    #child_project = ET.SubElement(project, "CHILD_PROJECT", accession=config.data['ebpnor.species']['adm:child_pro'])
    
    #tax_id = ET.SubElement(project, "TAXON_ID")
    #tax_id.text = config.data['ebpnor.species']['tax:id'].split(':')[1]
    #tax_sci_name = ET.SubElement(project, "SCIENTIFIC_NAME")
    #tax_sci_name.text = config.data['ebpnor.species']['tax:sci_name']
    submission_project = ET.SubElement(project, "SUBMISSION_PROJECT")
    sequencing_project = ET.SubElement(submission_project, "SEQUENCING_PROJECT")

    return root

def create_isolate_project_set_xml(config, root):
    '''
    Returns <PROJECT_SET> for isolate project and all subelements. This is explicily defined to mimic the Darwin tree of life structure in ENA
    '''
    project_set = ET.SubElement(root, "PROJECT_SET")
    project = ET.SubElement(project_set, "PROJECT", alias=config.data['ebpnor.isolate']['adm:proj_alias'], center_name=config.data['ebpnor.species']['adm:cent_name'])
    name = ET.SubElement(project, "NAME")
    name.text = config.data['ebpnor.isolate']['adm:proj_name']
    title = ET.SubElement(project, "TITLE")
    title.text = config.data['ebpnor.isolate']['adm:proj_title']
    description = ET.SubElement(project, "DESCRIPTION")
    description.text = config.data['ebpnor.isolate']['adm:proj_title']
    #parent_project = ET.SubElement(project, "PARENT_PROJECT", accession=config.data['ebpnor.species']['adm:parent_pro'])
    submission_project = ET.SubElement(project, "SUBMISSION_PROJECT")
    sequencing_project = ET.SubElement(submission_project, "SEQUENCING_PROJECT")

    return root

def create_sample_set_xml(config, root):
    '''
    Returns <SAMPLE_SET> and all subelements
    '''
    sample_set = ET.SubElement(root, "SAMPLE_SET")
    sample = ET.SubElement(sample_set, "SAMPLE", alias=config.data['ebpnor.specimen']['pbb:spe_name'])

    title = ET.SubElement(sample, "TITLE")
    title.text = "Submitted automatically through CBF at - {}".format(date.today())

    sample_name = ET.SubElement(sample, "SAMPLE_NAME")

    taxon_id = ET.SubElement(sample_name, "TAXON_ID")
    taxon_id.text = config.data['ebpnor.species']['tax:id'].split(':')[1]

    scientific_name = ET.SubElement(sample_name, "SCIENTIFIC_NAME")
    scientific_name.text = config.data['ebpnor.species']['tax:sci_name']

    sample_attributes = ET.SubElement(sample, "SAMPLE_ATTRIBUTES")
    # project_name
    sample_attribute = ET.SubElement(sample_attributes, "SAMPLE_ATTRIBUTE")
    tag = ET.SubElement(sample_attribute, "TAG")
    tag.text = "project name"
    value = ET.SubElement(sample_attribute, "VALUE")
    value.text = config.data['ebpnor.isolate']['adm:proj_title']
    # collected_by
    sample_attribute = ET.SubElement(sample_attributes, "SAMPLE_ATTRIBUTE")
    tag = ET.SubElement(sample_attribute, "TAG")
    tag.text = "collected_by"
    value = ET.SubElement(sample_attribute, "VALUE")
    value.text = config.data['ebpnor.isolate']['sam:coll_by']
    # collecting institution 
    sample_attribute = ET.SubElement(sample_attributes, "SAMPLE_ATTRIBUTE")
    tag = ET.SubElement(sample_attribute, "TAG")
    tag.text = "collecting institution"
    value = ET.SubElement(sample_attribute, "VALUE")
    value.text = config.data['ebpnor.isolate']['sam:coll_affi']
    # collection date 
    sample_attribute = ET.SubElement(sample_attributes, "SAMPLE_ATTRIBUTE")
    tag = ET.SubElement(sample_attribute, "TAG")
    tag.text = "collection date"
    value = ET.SubElement(sample_attribute, "VALUE")
    value.text = config.data['ebpnor.isolate']['sam:date']
    # geo location (semantic)
    sample_attribute = ET.SubElement(sample_attributes, "SAMPLE_ATTRIBUTE")
    tag = ET.SubElement(sample_attribute, "TAG")
    tag.text = "geographic location (country and/or sea)"
    value = ET.SubElement(sample_attribute, "VALUE")
    value.text = "Norway"
    # geo location (semantic)
    sample_attribute = ET.SubElement(sample_attributes, "SAMPLE_ATTRIBUTE")
    tag = ET.SubElement(sample_attribute, "TAG")
    tag.text = "geographic location (region and locality)"
    value = ET.SubElement(sample_attribute, "VALUE")
    value.text = config.data['ebpnor.isolate']['sam:location']
    # geo location (latitude)
    sample_attribute = ET.SubElement(sample_attributes, "SAMPLE_ATTRIBUTE")
    tag = ET.SubElement(sample_attribute, "TAG")
    tag.text = "geographic location (latitude)"
    units = ET.SubElement(sample_attribute, "UNITS")
    units.text = "DD"
    value = ET.SubElement(sample_attribute, "VALUE")
    value.text = config.data['ebpnor.isolate']['sam:coordinate'].split(':')[1].split(',')[0]
    # geo location (longitude)
    sample_attribute = ET.SubElement(sample_attributes, "SAMPLE_ATTRIBUTE")
    tag = ET.SubElement(sample_attribute, "TAG")
    tag.text = "geographic location (longitude)"
    units = ET.SubElement(sample_attribute, "UNITS")
    units.text = "DD"
    value = ET.SubElement(sample_attribute, "VALUE")
    value.text = config.data['ebpnor.isolate']['sam:coordinate'].split(':')[1].split(',')[1]
    # habitat
    sample_attribute = ET.SubElement(sample_attributes, "SAMPLE_ATTRIBUTE")
    tag = ET.SubElement(sample_attribute, "TAG")
    tag.text = "habitat"
    value = ET.SubElement(sample_attribute, "VALUE")
    value.text = config.data['ebpnor.isolate']['sam:habitat']
    # lifestage
    sample_attribute = ET.SubElement(sample_attributes, "SAMPLE_ATTRIBUTE")
    tag = ET.SubElement(sample_attribute, "TAG")
    tag.text = "lifestage"
    value = ET.SubElement(sample_attribute, "VALUE")
    value.text = config.data['ebpnor.isolate']['sam:lifestage']
    # sample_description
    sample_attribute = ET.SubElement(sample_attributes, "SAMPLE_ATTRIBUTE")
    tag = ET.SubElement(sample_attribute, "TAG")
    tag.text = "sample_description"
    value = ET.SubElement(sample_attribute, "VALUE")
    value.text = config.data['ebpnor.isolate']['sam:method']
    # sex
    sample_attribute = ET.SubElement(sample_attributes, "SAMPLE_ATTRIBUTE")
    tag = ET.SubElement(sample_attribute, "TAG")
    tag.text = "sex"
    value = ET.SubElement(sample_attribute, "VALUE")
    value.text = config.data['ebpnor.isolate']['sam:sex']
    # organism part
    sample_attribute = ET.SubElement(sample_attributes, "SAMPLE_ATTRIBUTE")
    tag = ET.SubElement(sample_attribute, "TAG")
    tag.text = "organism part"
    value = ET.SubElement(sample_attribute, "VALUE")
    value.text = config.data['ebpnor.specimen']['pbb:org_part']
    # ENA checklist
    sample_attribute = ET.SubElement(sample_attributes, "SAMPLE_ATTRIBUTE")
    tag = ET.SubElement(sample_attribute, "TAG")
    tag.text = "ENA-CHECKLIST"
    value = ET.SubElement(sample_attribute, "VALUE")
    value.text = config.data['ebpnor.specimen']['pbb:ena_chlst']
    # specimen voucher
    #sample_attribute = ET.SubElement(sample_attributes, "SAMPLE_ATTRIBUTE")
    #tag = ET.SubElement(sample_attribute, "TAG")
    #tag.text = "specimen_voucher"
    #value = ET.SubElement(sample_attribute, "VALUE")
    #value.text = config.data['ebpnor.specimen']['pbb:voucher_id']

    return root

def create_experiment_set(config, root):
    '''
    Returns <EXPERIMENT_SET> and all subelements
    '''
    experiment_set = ET.SubElement(root, "EXPERIMENT_SET")
    ### For each sequence technology (EXPERIMENT)
    for i, entry in config.data['ebpnor.sequencing'].items():
        experiment = ET.SubElement(experiment_set, "EXPERIMENT", alias=entry['seq:exp_alias'])
        #title = ET.SubElement(experiment, "TITLE")
        study_ref = ET.SubElement(experiment, "STUDY_REF", accession=config.data['ebpnor.isolate']['adm:proj_alias'])

        design = ET.SubElement(experiment, "DESIGN")
        design_description = ET.SubElement(design, "DESIGN_DESCRIPTION")
        sample_descriptor = ET.SubElement(design, "SAMPLE_DESCRIPTOR", accession=config.data['ebpnor.specimen']['pbb:spe_name'])
        library_descriptor = ET.SubElement(design, "LIBRARY_DESCRIPTOR")
        library_name = ET.SubElement(library_descriptor, "LIBRARY_NAME")
        library_strategy = ET.SubElement(library_descriptor, "LIBRARY_STRATEGY")
        library_strategy.text = entry['lib:strategy']
        library_source = ET.SubElement(library_descriptor, "LIBRARY_SOURCE")
        library_source.text = entry['lib:source']
        library_selection = ET.SubElement(library_descriptor, "LIBRARY_SELECTION")
        library_selection.text = entry['lib:select']
        library_layout = ET.SubElement(library_descriptor, "LIBRARY_LAYOUT")
        # Suddenly vital information is part of the TAG, so we need to do some checks
        if entry['lib:layout'].lower() == "paired":
            paired = ET.SubElement(library_layout, "PAIRED", NOMINAL_LENGTH=entry['lib:insert_size'])
        else:
            single = ET.SubElement(library_layout, "SINGLE")
        library_construction_protocol = ET.SubElement(library_descriptor, "LIBRARY_CONSTRUCTION_PROTOCOL")
        library_construction_protocol.text = entry['lib:protocol']

        platform = ET.SubElement(experiment, "PLATFORM")
        if entry['seq:platform'].lower() == "pacbio_smrt":
            tec = ET.SubElement(platform, "PACBIO_SMART")
        elif entry['seq:platform'].lower() == "bgiseq":
            tec = ET.SubElement(platform, "BGISEQ")
        elif entry['seq:platform'].lower() == "capillary":
            tec = ET.SubElement(platform, "CAPILLARY")
        elif entry['seq:platform'].lower() == "oxford_nanopore":
            tec = ET.SubElement(platform, "OXFORD_NANOPORE")
        elif entry['seq:platform'].lower() == "illumina":
            tec = ET.SubElement(platform, "ILLUMINA")
        else:
            exit("seq:platform not supported!")

        #illumina = ET.SubElement(platform, "ILLUMINA")
        instument_model = ET.SubElement(tec, "INSTRUMENT_MODEL")
        instument_model.text = entry['seq:inst_model']

    return root

def create_run_set(config, root):
    '''
    Returns <RUN_SET> and all subelements
    '''
    run_set = ET.SubElement(root, "RUN_SET")
    ### For each sequence technology (RUN)
    for i, entry in config.data['ebpnor.sequencing'].items():
        run = ET.SubElement(run_set, "RUN", alias=entry['seq:run_alias'])
        experiment_ref = ET.SubElement(run, "EXPERIMENT_REF", refname=entry['seq:exp_ref'])
        data_block = ET.SubElement(run, "DATA_BLOCK")
        files = ET.SubElement(data_block, "FILES")
        ### For each file (from sequence technology) (FILE)
        for filename, chksum in zip(entry['seq:filename'], entry['seq:chk_sum']):
            file = ET.SubElement(files, "FILE", filename=filename, filetype=entry['seq:filetype'].lower(), checksum_method=entry['seq:chk_sum_mt'], checksum=chksum)
    return root

def write_xml(xml, config, filename):
    # Write XML
    xml.write(os.path.join(config.output_folder, filename))
    # Write XML indented
    filename = "indented."+filename
    with open(os.path.join(config.output_folder, filename), "w") as file:
        xml = ET.tostring(xml.getroot())
        file.write(BeautifulSoup(xml, "xml").prettify())
